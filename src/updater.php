<?php

/**
 * @package Boldface\LeahTherapy
 */
declare( strict_types = 1 );
namespace Boldface\LeahTherapy;

/**
 * Class for updating the theme.
 *
 * @since 1.0
 */
class updater extends \Boldface\Bootstrap\updater {
  protected $theme = 'leah-therapy';
  protected $repository = 'boldface/leah-therapy';
}
