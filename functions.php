<?php

/**
 * @package Boldface\LeahTherapy
 */
declare( strict_types = 1 );
namespace Boldface\LeahTherapy;

/**
 * Use auto updater.
 *
 * @since 1.0
 */
function init() {
  require __DIR__ . '/src/updater.php';
  $updater = new updater();
  $updater->init();
}
\add_action( 'init', __NAMESPACE__ . '\init' );

/**
 * Filter the modules list.
 *
 * @since 1.0
 *
 * @param array $list List of modules.
 *
 * @return array The new list of modules.
 */
function modules( array $list ) : array {
  $list[] = 'featuredImage';
  $list[] = 'googleFonts';
  $list[] = 'images';
  $list[] = 'jumbotron';
  $list[] = 'widgets';
  return $list;
}
\add_filter( 'Boldface\Bootstrap\Controllers\modules', __NAMESPACE__ . '\modules' );

/**
 * Add footer widget.
 *
 * @since 1.0
 *
 * @param array Array of sidebars.
 *
 * @return array The filtered array of sidebars.
 */
function sidebars( array $sidebars ) : array {
  $sidebars[] = [
    'id' => 'footer',
    'name' => 'Footer',
    'before_widget' => '<div class="col-sm %2$s">',
    'after_widget' => '</div>',
  ];
  return $sidebars;
}
\add_filter( 'Boldface\Bootstrap\Models\widgets\sidebars', __NAMESPACE__ . '\sidebars' );

/**
 * Add the footer widget to the footer.
 *
 * @since 1.0
 *
 * @param string $html The footer HTML.
 *
 * @return string The new footer HTML.
 */
function footer( string $html ) : string {
  if( ! \is_active_sidebar( 'footer' ) ) {
    return $html;
  }
  ob_start();
  \dynamic_sidebar( 'footer' );
  $sidebar = ob_get_clean();
  return sprintf(
    '<aside class="container-fluid bg-grey"><div class="container"><div class="row">%1$s</div></div></aside>%2$s',
    $sidebar,
    $html
  );
}
\add_filter( 'Boldface\Bootstrap\Views\footer', __NAMESPACE__ . '\footer', 20 );

/**
 * Filter the footer class.
 *
 * @since 1.0
 *
 * @param string $class The unfiltered footer class.
 *
 * @return string The filtered footer class.
 */
function footerClass( string $class ) : string {
  $class = str_replace( 'bg-light', 'bg-dark text-white', $class );
  return str_replace( ' fixed-bottom', '', $class );
}
\add_filter( 'Boldface\Bootstrap\Views\footer\class', __NAMESPACE__ . '\footerClass' );

/**
 * Filter the loop class.
 *
 * @since 1.0
 *
 * @param string The unfiltered loop class.
 *
 * @return string The filtered loop class.
 */
function loopClass( string $class ) : string {
  return \is_front_page() ? 'container-fluid' : 'container';
}
\add_filter( 'Boldface\Bootstrap\Views\loop\class', __NAMESPACE__ . '\loopClass' );

/**
 * Filter the entry header.
 *
 * @since 1.0
 *
 * @param string $header The unfiltered entry header.
 *
 * @return string The filtered entry header.
 */
function entryHeader( string $header ) : string {
  if( \is_singular( 'post' ) ) {
    $header .= sprintf( '<div class="row"><div class="col-md">Categories: %1$s</div></div>', \get_the_category_list( ', ' ) );
    $header .= sprintf( '<div class="row"><div class="col-md">%1$s</div></div>', \get_the_date() );
  }
  if( \is_home() || \is_singular( 'post' ) ) {
    return $header . sprintf( '<div class="row"><div class="col-md">%1$s</div></div>', \get_the_post_thumbnail() );
  }
  if( \is_page() ) {
    return $header;
  }
  return \is_front_page() ? '' : sprintf(
    '%1$s<div class="row"><div class="col-md">Categories: %2$s</div></div>',
    $header,
    \get_the_category_list( ', ' )
  );
}
\add_filter( 'Boldface\Bootstrap\Views\entry\header', __NAMESPACE__ . '\entryHeader' , 200 );

/**
 * Remove the categories from the footer.
 *
 * @since 1.0.1
 *
 * @param string $footer The unfiltered entry footer.
 *
 * @return string The filtered entry footer.
 */
function removeCategoriesFromFooter( string $footer ) : string {
  $start = strpos( $footer, '<p>Categories: ' );
  if( false === $start ) {
    return $footer;
  }
  $stop = strpos( $footer, '</p>', $start );
  return substr_replace( $footer, '', $start, $stop - $start + 4 );
}
\add_filter( 'Boldface\Bootstrap\Views\entry\footer', __NAMESPACE__ . '\removeCategoriesFromFooter', 20 );

/**
 * Filter the entry content on posts page.
 *
 * @since 1.0
 *
 * @param string $content The unfiltered entry content.
 *
 * @return string The filtered entry content on posts page.
 */
function entryContent( string $content ) : string {
  return \is_home() ? sprintf( '<div class="row"><div class="col-md">%1$s</div></div>', \get_the_excerpt() ) : $content;
}
\add_filter( 'Boldface\Bootstrap\Views\entry\content', __NAMESPACE__ . '\entryContent', 200 );

/**
 * Return the metaslider shortcode portion of the content.
 *
 * @since 1.0
 *
 * @param string $content The HTML content.
 *
 * @return string The metaslider shortcode portion of the content.
 */
function parseMetaSlider( string $content ) : string {
  $pos = strpos( $content, '[metaslider ' );
  if( false === $pos ) {
    return '';
  }
  $end = strpos( $content, ']', $pos + 1 );
  return substr( $content, $pos, $end - $pos + 1 );
}

/**
 * Remove the metaslider shortcode from the content.
 *
 * @since 1.0
 *
 * @param string $content The HTML content.
 *
 * @return string The content with the metaslider shortcode removed.
 */
function removeMetaSlider( string $content ) : string {
  $pos = strpos( $content, '[metaslider ' );
  if( false === $pos ) {
    return $content;
  }
  $end = strpos( $content, ']', $pos + 1 );
  $str = substr( $content, $pos, $end - $pos + 1 );
  return str_replace( $str, '', $content );
}

/**
 * Add the meta slider to the jumbotron on the front page.
 *
 * @since 1.0
 *
 * @param string $jumbotron The jumbotron view.
 *
 * @return string The filtered jumbotron.
 */
function jumbotron( string $jumbotron ) : string {
  if( ! \is_front_page() ) {
    return '';
  }
  $content = \get_page( \get_the_ID() )->post_content;
  $pos = strpos( $content, '[metaslider ' );
  if( false === $pos ) {
    return $jumbotron;
  }
  $end = strpos( $content, ']', $pos + 1 );
  \add_filter( 'the_content', __NAMESPACE__ . '\removeMetaSlider' );
  return \do_shortcode( substr( $content, $pos, $end - $pos + 1 ) );
}
\add_filter( 'Boldface\Bootstrap\Views\jumbotron', __NAMESPACE__ . '\jumbotron', 20 );

/**
 * Add Google fonts.
 *
 * @since 1.0
 *
 * @param array $fonts An array of Google fonts. Unused.
 *
 * @return array The new array of Google fonts.
 */
function googleFonts( array $fonts ) : array {
  return [ 'Didact Gothic' ];
}
\add_filter( 'Boldface\Bootstrap\Models\googleFonts', __NAMESPACE__ . '\googleFonts' );

/**
 * Return the HTML of the 3 most recent posts.
 *
 * @since 1.0
 *
 * @return string The HTML of the 3 most recent posts.
 */
function getRecentPosts() : string {

  $args = [
    'post_type'      => 'post',
    'posts_per_page' => 3,
  ];
  if( \is_singular( 'post' ) ) {
    $args[ 'post__not_in' ] = [ \get_the_ID() ];
  }

  $query = new \WP_Query( $args );
  if( ! $query->have_posts() ) {
    return '';
  }

  $posts = '<header class="col-md"><h4 class="grey">You might also like</h4></header>';
  while( $query->have_posts() ) {
    $query->the_post();
    $posts .= sprintf(
      '<article class="container">
      <header class="col-md">
      <h2><a href="%2$s">%3$s</a></h2>%1$s</a>
      <p>%4$s</p>
      </header>
      %5$s
      <footer class="row"><div class="col-md">%6$s</div></footer>
      </article>',
      \has_post_thumbnail() ? \get_the_post_thumbnail() : '',
      \get_the_permalink(),
      \get_the_title(),
      \get_the_date(),
      \apply_filters( 'the_content', \get_the_excerpt() ),
      '<a href="'.\get_the_permalink().'">Continue Reading</a>'
    );
  }
  return sprintf( '<section class="container-fluid bg-white recent-posts">%1$s</section>', $posts );
}

/**
 * Filter the entry views.
 *
 * @since 1.0
 *
 * @param string $entry The unfilted entry views.
 *
 * @return string The filtered entry views.
 */
function entryRecentPosts( string $entry ) : string {
  if( \is_home() || \is_front_page() || ! \is_singular( 'post' ) ) {
    return $entry;
  }
  return $entry . getRecentPosts();
}
\add_filter( 'Boldface\Bootstrap\Views\entry', __NAMESPACE__ . '\entryRecentPosts', 20 );

/**
 * Filter the attachment image attributes.
 *
 * @since 1.0
 *
 * @param array     $attr       Attachment attributes.
 * @param WP_Post   $attachment The WP_Post object.
 * @param array|int $size       The size of the attachment.
 *
 * @return array The filters attachment attributes.
 */
function wp_get_attachment_image_attributes( array $attr, \WP_Post $attachment, $size ) : array {
  $attr[ 'class' ] = 'pb-4';
  return $attr;
}
\add_filter( 'wp_get_attachment_image_attributes', __NAMESPACE__ . '\wp_get_attachment_image_attributes', 10, 3 );

/**
 * Print the custom navigation callback.
 *
 * @since 1.0
 */
function navigation() {
  printf(
'<nav class="%1$s">
<div class="container">
<a class="navbar-brand" href="%2$s">%3$s</a>
<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
%4$s
</div>
</nav>',
    \apply_filters( 'Boldface\Bootstrap\Views\navigation\class', 'navbar navbar-expand-md navbar-dark bg-dark fixed-top' ),
    \home_url('/'),
    \apply_filters( 'Boldface\Bootstrap\Views\navigation\brand', \get_bloginfo( 'name' ) ),
    \apply_filters( 'Boldface\Bootstrap\Views\navigation\menu', '' )
  );
}

/**
 * Filter the navigation callback.
 *
 * @since 1.0
 *
 * @param callable $callback The navigation callback. Unused.
 *
 * @return callable The filtered navaigation callback.
 */
function navigationCallback( callable $callback ) : callable {
  return __NAMESPACE__ . '\navigation';
}
\add_filter( 'Boldface\Bootstrap\Controllers\navigation\callback', __NAMESPACE__ . '\navigationCallback' );

/**
 * Filter the navigation class.
 *
 * @since 1.0
 *
 * @param string $class The unfiltered navigation class.
 *
 * @return string The filtered navigation class.
 */
function navigationClass( string $class ) : string {
  $class = str_replace( [ 'bg-dark', 'navbar-dark' ], [ 'bg-white', 'navbar-light' ], $class );
  return $class . ' navbar-primary navbar-large';
}
\add_filter( 'Boldface\Bootstrap\Views\navigation\class', __NAMESPACE__ . '\navigationClass' );

/**
 * Filter the entry class.
 *
 * @since 1.0
 *
 * @param string $class The unfiltered entry class.
 *
 * @return string The filtered entry class.
 */
function entryClass( string $class ) : string {
  return 'container ' . $class;
}
\add_filter( 'Boldface\Bootstrap\Views\entry\class', __NAMESPACE__ . '\entryClass' );

/**
 * Filter the footer text.
 *
 * @since 1.0
 *
 * @param string $text The unfiltered footer text. Unused.
 *
 * @return string The new footer text.
 */
function footerText( string $text ) : string {
  return sprintf( '&copy; Copyright %1$s. All rights reserved.', date( 'Y' ) );
}
\add_filter( 'Boldface\Bootstrap\Views\footer\text', __NAMESPACE__ . '\footerText' );

/**
 * Filter the excerpt by appending a continue reading link.
 *
 * @since 1.0
 *
 * @param string $excerpt The unfiltered excerpt.
 *
 * @return string The filtered excerpt.
 */
function get_the_excerpt( string $excerpt ) : string {
  if( ! \in_the_loop() ) {
    return $excerpt;
  }
  return $excerpt . sprintf(
    '<div class="row"><div class="col-md-12"><a href="%1$s">Continue Reading</a></div></div>',
    \get_the_permalink()
  );
}
//\add_filter( 'get_the_excerpt', __NAMESPACE__ . '\get_the_excerpt', 50 );

/**
 * Return the post thumbnail appended to the header.
 *
 * @since 1.0
 *
 * @param string $header The header.
 *
 * @return string The post thumbnail appended to the header.
 */
function featuredImage( string $header ) : string {
  return $header . \get_the_post_thumbnail();
}

/**
 * Filter the featured image callback.
 *
 * @since 1.0
 *
 * @param callable $callback The unfiltered featured image callback.
 *
 * @return callable The filtered featured image callback.
 */
function featuredImageCallback( callable $callback ) : callable {
  \add_action( 'wp', function() use ( $callback ) {
    \remove_action( 'wp', $callback, 20 );
  }, 15 );
  return $callback;
}
\add_filter( 'Boldface\Bootstrap\Controllers\featuredImage\callback', __NAMESPACE__ . '\featuredImageCallback' );

/**
 * Filter the contactForm7 button class.
 *
 * @since 1.0
 *
 * @param string $class The unfiltered contactForm7 button class.
 *
 * @return string The filtered contactForm7 button class.
 */
function buttonClass( string $class ) : string {
  return str_replace( 'btn-primary', 'btn-secondary', $class );
}
\add_filter( 'Boldface\Bootstrap\Models\contactForm7\button\class', __NAMESPACE__ . '\buttonClass' );

\add_filter( 'Boldface\Bootstrap\REST', '__return_false' );

\add_filter( 'run_wptexturize', '__return_false' );

\add_filter( 'Boldface\Bootstrap\localAssets', '__return_true' );
